---
title: Ross Larson
subtitle: "Speaker. Human Being."
---
[![Ross Larson](/img/ross.png)](https://ross-larson.github.io)

Ross Larson became a software developer through the nontraditional path of obtaining a double major in Psychology and French from Luther College. He later obtained a software development degree from Fox Valley Technical College.  He works as a Software Developer for Paradigm in Middleton, WI.  He has volunteered teaching computer science at a local high school through the [TEALS program](https://www.tealsk12.org/).  He enjoys indie and retro video games, board games, the NFL Draft, creating obscure fantasy leagues, and making charts.

Ross has previously spoken at meetups and conferences like [That Conference](https://old.thatconference.com/speakers/speaker/RossLarson).

Ross earned the [MCSA: Web Applications](https://www.youracclaim.com/badges/466c91b6-14c1-406e-b0b3-5ab3e519cc27/linked_in_profile) certification in 2019.

[![MCSA](/img/MCSA-Web.png)](https://www.youracclaim.com/badges/466c91b6-14c1-406e-b0b3-5ab3e519cc27/linked_in_profile)
