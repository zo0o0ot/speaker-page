---
title: Coding inside the web browser! Talking about GitHub Codespaces and GitPod.
date: "2021-06-27T19:33:20Z"
url: "/slides/coding-in-the-web/"
image: "/talks/that_conference_10yr_logo.svg"
description: "Slides for THAT Conference 2021 talk about coding inside a web browser."
ratio: "16:9"
themes:
- apron
- adirondack
- descartes
classes:
- feature-qrcode
---
class: title, smokescreen, shelf, no-footer
background-image: url(gitpod/coding-web-gitpod-screenshot.png)

# Coding in the Web
## An introduction to GitPod and GitHub Codespaces

???

Make sure you're in the right room!
---
background-image: url(that/That-Conference-Branding-Slide.png)
background-size: cover

???

Temporary Branding slide- 2021 slide TBD

Welcome to THAT Conference

- Background images don't appear to scale correctly unless I also apply "background-size: cover" to the slide.-

---
background-image: url(that/That-Conference-Sponsors-Slide.png)
background-size: cover

???

Temporary Sponsors slide- 2021 slide TBD

Sponsors are great.  Without them, That Conference would not be possible.  I'm thankful for them.

---
background-image: url(that/That-Conference-2020.png)
background-size: cover

???

Temporary Future Dates slide- 2021-2022 slide TBD

Please consider coming back next year. Here are the dates!

---
class: img-left
# About Me

![Selfie](ross1.jpg)

- Software Developer at Paradigm
- Luther College Alumnus
- Admin, [Madison, WI Slack](http://madisoncommunity.org/)
- Human being
- Survivor

@rosslarsonWI

hello@rosslarson.com

THAT Slack : rosslarson

???

Hi. I'm Ross.

I'm a father, a gamer, a sports fan, a geek, and other stuff.
---
# Slides and Session Information

.qrcode.db.fr.w-40pct.ml-4[]

- General session info available at https://rosslarson.com/talks/
- Slides are at https://rosslarson.com/slides/ or just use the QR code
- Ask questions anytime



