---
title: 'Coding inside the web browser! Talking about GitHub Codespaces and GitPod.'
date: "2021-06-27T19:03:06Z"
url: "/talks/coding-in-the-web-gitpod-gh-codespaces/"
event: "That Conference 2021"
location: "Kalahari - Wisconsin Dells, WI"
site: "https://that.us/activities/BGYRTF2TQE816s9NDL8d"
video: ""
slides: "/slides/coding-in-the-web/"
thumbnail: "/talks/that_conference_10yr_logo.svg"
image: "/talks/hallway-that-top.jpg"
description: "Coding inside the web browser! Talking about GitHub Codespaces and GitPod."
---
## THAT Conference 2021

Day: Tuesday, July 27   Time: 10:30 AM  

Are you working on a side project and aren't close to your development machine? If you have access to a web browser, you can code right now if you develop in the cloud!

As cloud resources and web-based source code management tools improve in quality and affordability over time, the days of needing a dedicated development machine may no longer be necessary for all development workflows.
This is a discussion on developing in the browser, with me showing several examples of projects that I've worked on in GitPod and GitHub Codespaces.
I will then give some instructions on how to set up your own cloud development environment!

Goals:
At the end of this session, here are some key takeaways:
* Gain a better understanding of cloud-based development in the web browser.
* Know the questions to ask and the steps to take to get started coding in the web.